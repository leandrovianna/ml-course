// Package linreg defines functions of Machine Learning's
// Linear Regression algorithm.
package linreg

import "math"

// LinearRegression execute Linear Regression algorithm
// and returns the theta variables (hypothesis function parameters)
// that minimize the cost function.
// x refers to input variables or features.
// y refers to output variable or target variable.
// This implementation use Gradient Descent to minimize
// the cost function.
func LinearRegression(x [][]float64, y []float64, alpha float64, niter int) ([]float64, []float64) {
	costHistory := make([]float64, niter)
	theta := make([]float64, len(x[0]))

	for i := 0; i < niter; i++ {
		sigma := gradientDescent(x, y, theta)
		theta = updateParameters(theta, sigma, alpha)
		costHistory[i] = computeCost(x, y, theta)
	}

	return theta, costHistory
}

// computeCost compute and returns the cost function (mean sq. error)
// of Linear Regression model with hypothesis parameters (theta slice)
// and target variable passed (y slice).
func computeCost(x [][]float64, y, theta []float64) float64 {
	m := len(x) // number of examples

	var value = 0.0
	for i := range x {
		value += math.Pow(ComputeHypothesis(x[i], theta)-y[i], 2)
	}

	return value / (2.0 * float64(m))
}

// gradientDescent calculates the gradient descent for each
// hypothesis variable (theta slice)
func gradientDescent(x [][]float64, y, theta []float64) []float64 {
	m := len(x) // number of examples
	sigma := make([]float64, len(theta))

	for i := range x {
		errorValue := ComputeHypothesis(x[i], theta) - y[i]
		for j := range sigma {
			sigma[j] += errorValue * x[i][j]
		}
	}

	for j := range sigma {
		sigma[j] /= float64(m)
	}

	return sigma
}

// updateParameters returns the hypothesis parameters (theta slice)
// updated using the learning rate (alpha) and the
// gradient of cost function (sigma slice).
func updateParameters(theta, sigma []float64, alpha float64) []float64 {
	theta2 := make([]float64, len(theta))
	for j := range theta2 {
		theta2[j] = theta[j] - alpha*sigma[j]
	}
	return theta2
}

// computeHypothesis returns the value of hypothesis function
// using the parameters (theta slice) passed for a example (x slice).
func ComputeHypothesis(x, theta []float64) float64 {
	value := 0.0
	for i := range x {
		value += x[i] * theta[i]
	}
	return value
}

// NormalizeData apply feature scaling on the examples (slice x).
// Make features have approximately zero mean and scale to range
// [-1, 1]. The mean and the range of values are returned too.
func NormalizeData(x [][]float64) ([][]float64, []float64, []float64) {
	w := make([][]float64, len(x))

	m := len(x)    // number of examples
	n := len(x[0]) // number of features

	avg := make([]float64, n)
	max := make([]float64, n)
	min := make([]float64, n)

	for j := 0; j < n; j++ {
		max[j] = math.Inf(-1)
		min[j] = math.Inf(1)
	}

	for i := range w {
		w[i] = make([]float64, len(x[i]))

		for j := range w[i] {
			avg[j] += x[i][j]
			max[j] = math.Max(max[j], x[i][j])
			min[j] = math.Min(min[j], x[i][j])
		}
	}

	for j := 0; j < n; j++ {
		avg[j] /= float64(m)
	}

	std := make([]float64, n)
	for j := range std {
		std[j] = max[j] - min[j]
	}

	for i := range w {
		for j := range w[i] {
			if max[j] != min[j] {
				w[i][j] = (x[i][j] - avg[j]) / (std[j])
			} else {
				w[i][j] = x[i][j]
			}
		}
	}

	return w, avg, std
}
