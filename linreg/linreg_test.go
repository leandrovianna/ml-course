package linreg

import (
	"bufio"
	"math"
	"os"
	"strconv"
	"testing"
)

const (
	epsilon = 1e-9
)

func almostEqual(a, b float64) bool {
	return almostEqualBy(a, b, epsilon)
}

func almostEqualBy(a, b, epsilon float64) bool {
	return math.Abs(a-b) < epsilon
}

func TestLinearRegressionModel1(t *testing.T) {
	// input variables, features
	X := [][]float64{
		[]float64{1, 1},
		[]float64{1, 2},
		[]float64{1, 3},
	}

	// output variables, target variable
	y := []float64{1, 2, 3}

	// run the model and get the hypothesis parameters
	theta, _ := LinearRegression(X, y, 0.3, 600)

	if !almostEqual(theta[0], 0) || !almostEqual(theta[1], 1) {
		t.Errorf("Hypothesis parameter is wrong, got %v, expected %v", theta, []float64{0, 1})
	}
}

func TestLinearRegressionModel2(t *testing.T) {
	// input variables, features
	x := [][]float64{
		[]float64{1, 1},
		[]float64{1, 2},
		[]float64{1, 3},
	}

	// output variables, target variable
	y := []float64{2, 4, 6}

	// run the model and get the hypothesis parameters
	theta, _ := LinearRegression(x, y, 0.3, 600)

	if !almostEqual(theta[0], 0) || !almostEqual(theta[1], 2) {
		t.Errorf("Hypothesis parameter is wrong, got %v, expected %v", theta, []float64{0, 2})
	}
}

func TestLinearRegressionModel3(t *testing.T) {
	// input variables, features
	var x [][]float64

	// output variables, target variable
	var y []float64

	file, err := os.Open("../data/houses.txt")
	if err != nil {
		t.Fatal(err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		for _, v := range data {
			advance++
			if v == ',' || v == '\n' {
				return
			} else {
				token = append(token, v)
			}
		}

		return
	})

	toFloat64 := func(text string) float64 {
		if v, err := strconv.ParseFloat(text, 64); err != nil {
			return 0.0
		} else {
			return v
		}
	}

	for i := 0; scanner.Scan(); i++ {
		x = append(x, []float64{1})

		x[i] = append(x[i], toFloat64(scanner.Text()))
		scanner.Scan()
		x[i] = append(x[i], toFloat64(scanner.Text()))
		scanner.Scan()
		y = append(y, toFloat64(scanner.Text()))
	}

	x, mean, std := NormalizeData(x)

	// run the model and get the hypothesis parameters
	const alpha = 1.0
	const niter = 50

	theta, costHistory := LinearRegression(x, y, alpha, niter)

	for i := 1; i < len(costHistory); i++ {
		if costHistory[i-1] < costHistory[i] {
			t.Errorf("No convergence, iteration %d the cost increases", i)
			break
		}
	}

	t.Logf("Minimum cost: %.2f", costHistory[len(costHistory)-1])

	// example of house to predicted price
	// predicted price
	price := ComputeHypothesis(
		[]float64{
			1,
			(1650 - mean[1]) / std[1],
			(3 - mean[2]) / std[2]},
		theta)

	t.Logf("House price predicted: %.3f", price)
}
