# ml-course #

Implementation of Machine Learning algorithm presented
on Andrew Ng's ML Course. The language used is Go.

## Week 1 and 2 - Linear Regression
- Package linreg
- Only Gradient Descent implementation
